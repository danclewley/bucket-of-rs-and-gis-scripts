#!/usr/bin/env python

from distutils.core import setup
import glob

scriptsList = glob.glob('*.py')

setup(name='UAVSAR_Reader',
      author = 'Nathan Thomas',
      author_email = 'nmt8@aber.ac.uk',
      scripts=scriptsList)
