# -*- coding: utf-8 -*-
''' Author: Nathan Thomas
    Email: nathan.m.thomas@nasa.gov, @DrNASApants
    Date: 03/23/2020
    Version: 1.0
    Copyright 2019 Natha M Thomas
    
    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:
    
    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.
    
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.'''

import argparse
import urllib.request
import subprocess
import os
import time


def SearchGEDI(product, version, bbox):
    # Convert bb to comma separated string
    bbox_str = ",".join(bbox)
    # The URL to the LP DAAC GEDI Finder Tool
    LPDAAC_URL = " https://lpdaacsvc.cr.usgs.gov/services/gedifinder?"
    # Construct the search parameter from the input arguments
    URL = LPDAAC_URL + 'product=' + product + '&version=' + version + '&bbox=' + bbox_str + '&output=html'
    # Open and read the GEDI Finder search results
    OpenURL = urllib.request.urlopen(URL)
    html = OpenURL.read()

    return html

def ListH5Files(HTML):
    # Decode the HTML text to String
    GEDIurl = HTML.decode('utf-8')

    # Create a list to store GEDI files
    h5files = []
    # Create an initial split on the text
    Initial = GEDIurl.split('</a>')
    # Iterate over each element and perform further splits and index the text needed
    # NOT VERY ELEGANT
    for i in range(len(Initial)):
        try:
            h5files.append(Initial[i].split('<a')[1].split('>')[1])
        except:
            # Due to the way it splits the text some lists have less than the required
            # index values but these do not contain H5 files
            pass

    return h5files

def DownloadGEDI(files, outpath, username, password, daterange):
    

    print("\n" + str(len(files)) + " GEDI H5 FILES FOUND\n")
    time.sleep(5)
    if daterange is not None:
        # Convert date string to julian date
        startdate =  time.strptime(daterange[0], "%Y-%m-%d")
        enddate =  time.strptime(daterange[1], "%Y-%m-%d")
        # start date
        sd = int(time.strftime("%Y%j",startdate))
        # end date
        ed = int(time.strftime("%Y%j",enddate))
        
        # Iterate over each H5 in the list and pull it using wget
        date_qualified = []
        for file in files:
            filename = file.split('/')[-1]
            filedate = filename.split('_')[2][:7]
            
            if int(filedate) >= sd and int(filedate) <= ed:
                date_qualified.append(file)

        print("\nTHERE ARE " + str(len(date_qualified)) + " GEDI H5 FILES THAT MATCH YOUR DATERANGE\n")
        time.sleep(5)

        if len(date_qualified) > 0:
            for file in date_qualified:
                newname = file.split('/')[-1]
                print('wget --user ' + username + ' --password ' + password + ' -O ' + os.path.join(outpath, newname) + ' '  + file)
                subprocess.call('wget --user ' + username + ' --password ' + password + ' -O ' + os.path.join(outpath, newname) + ' '  + file + ' --no-check-certificate',shell=True)

    else:
        for file in files:
            newname = file.split('/')[-1]
            print('wget --user ' + username + ' --password ' + password + ' -O ' + os.path.join(outpath, newname) + ' '  + file)
            subprocess.call('wget --user ' + username + ' --password ' + password + ' -O ' + os.path.join(outpath, newname) + ' '  + file + ' --no-check-certificate',shell=True)
    

def main():
    print("SearchPullGEDI.py is written by Nathan Thomas (nathan.m.thomas@nasa.gov, @DrNASApants) to search and pull GEDI data that intersects with a bounding box. This relies on the existing LP DAAC GEDI Finder Service: https://lpdaac.usgs.gov/documents/591/GEDIFinder_UserGuide_v1.0.pdf  \n\nExample: python SearchPullGEDI.py -p GEDI02_B -v 001 -bb -40.9218 -73.7731 -42.0248 -71.7736 -o /Users/Me/MyData/GEDI -u EarthDataUsername -pw EarthDataLogin -d 2019-01-01 2019-04-30 \n\nUse '-h' for help and required input parameters\n")
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--product", type=str, help="Specify the GEDI product required (eg. GEDI02_B)", required=True)
    parser.add_argument("-v", "--version", type=str, help="Specify the GEDI product version required", required=True)
    parser.add_argument("-bb", "--bbox", nargs=4, help="Specify the bounding box (ULlon ULlat LRlon LRlat)", required=True)
    parser.add_argument("-o", "--output", type=str, help="Specify the output GEDI .h5 path", required=True)
    parser.add_argument("-u", "--username", type=str, help="Specify your Earthdata username", required=True)
    parser.add_argument("-pw", "--password", type=str, help="Specify your Earthdata password", required=True)
    parser.add_argument("-d", "--date", nargs=2, help="Specify your date range (start data, end date; format YYYY-MM-DD)", required=False, default=None)
    
    args = parser.parse_args()
    
    if args.date is None:
        print("FYI: NO DATE RANGE HAS BEEN SPECIFIED (NOT MANDATORY)\n")

    HTML = SearchGEDI(args.product, args.version, args.bbox)

    GEDIfiles = ListH5Files(HTML)

    DownloadGEDI(GEDIfiles, args.output, args.username, args.password, args.date)

if __name__ == "__main__":
    main()



