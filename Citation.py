#! /usr/bin/env python


''' Author: Nathan Thomas
Ammended to process UAVSAR InSAR pairs by Yang Zheng (April 2015)
Email: nmt8@aber.ac.uk
Date: 23/08/2014
Version: 1.0
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.'''


import os.path
import sys
import glob
import argparse

    
    

def genHDRfromTXT(args):

    # Set up dictionary to hold header parameters
    headerPar = {}
    AuthorList = []
    file = args.input
    risFile = open(file, 'r')
    Title = ''
    Journal = ''
    Volume = ''
    Issue = ''
    Year = ''
    StartPage = ''
    EndPage = ''
    for line in risFile:
        if 'T1' in line:
            Title = line.split(' - ')[-1].strip() + '.'
        elif 'A1' in line:
            temp = line.split(',')[-1].strip().split()
            tempA = [x[0] for x in temp]
            Author = line.split(' - ')[-1].split(',')[0].strip() + ', ' + '. '.join(tempA) + '., '
            AuthorList.append(Author)
            Last = AuthorList[-1].split('.')[0]
            finalAuthors = AuthorList[0:-1]
            finalAuthors.append(Last)
        elif 'Y1' in line:
            Year = line.split(' - ')[-1].strip()
        elif 'JO' in line:
            Journal = line.split(' - ')[-1].strip()
        elif 'VL' in line:
            Volume = line.split(' - ')[-1].strip()
        elif 'IS' in line:
            Issue = line.split(' - ')[-1].strip()
        elif 'SP' in line:
            StartPage = line.split(' - ')[-1].strip()
        elif 'EP' in line:
            EndPage = line.split(' - ')[-1].strip()

    print(' '.join(finalAuthors) + '. (' + Year + ') ' + Title + ' ' + Journal + ', ' + Volume + ' (' + Issue + ')' + ', pp.' + StartPage + '-' + EndPage)

    if Title == '':
        print('\n TITLE MISSING')
    if Journal == '':
        print('\n JOURNAL MISSING')
    if Volume == '':
        print('\n VOLUME MISSING')
    if Issue == '':
        print('\n ISSUE MISSING')
    if Year == '':
        print('\n YEAR MISSING')
    if StartPage == '':
        print('\n START PAGE MISSING')
    if EndPage == '':
        print('\n END PAGE MISSING')


    print('\nThank you for using Citation.py\n')

def main():
    print("UAVSAR.py is written by Nathan Thomas (nmt8@aber.ac.uk, @Nmt28) of the Aberystwyth University Earth Observation and Ecosystems Dynamics Laboratory (@AU_EarthObs) as part of a visiting research program at NASA JPL. Ammended to process UAVSAR InSAR pair data by Yang Zheng (April 2015).\nUse '-h' for help and required input parameters\n")
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", type=str, help="Specify the input UAVSAR ann file")
    args = parser.parse_args()

    if '.ris' in str(args.input):
        pass
    else:
        print("INPUT UAVSAR ANN FILE MUST BE '.ris'")
        os._exit(1)
    if args.input == None:
        print("SPECIFY INPUT RIS FILE")
        os._exit(1)


    genHDRfromTXT(args)


if __name__ == "__main__":
    main()


