from datetime import date
from dateutil.relativedelta import relativedelta
import numpy as np
from osgeo import gdal
import argparse
import os

def GetDate(args):
    if args.sensor=='JERS':
        ds = gdal.Open(args.input)
        myarray = np.array(ds.GetRasterBand(1).ReadAsArray())
        uniqueVals = np.unique(myarray)
        Start = date(1992,2,11)
        for i in range(np.shape(uniqueVals)[0]):
            print('Value='+str(uniqueVals[i]), ':', 'Date='+str(Start+relativedelta(days=+int(uniqueVals[i])))+'\n')
    elif args.sensor=='ALOS':
        ds = gdal.Open(args.input)
        myarray = np.array(ds.GetRasterBand(1).ReadAsArray())
        uniqueVals = np.unique(myarray)
        Start = date(2010,1,24)
        for i in range(np.shape(uniqueVals)[0]):
            print('Value='+str(uniqueVals[i]), ':', 'Date='+str(Start+relativedelta(days=+int(uniqueVals[i])))+'\n')
    elif args.sensor=='ALOS2':
        ds = gdal.Open(args.input)
        myarray = np.array(ds.GetRasterBand(1).ReadAsArray())
        uniqueVals = np.unique(myarray)
        Start = date(2014,5,24)
        for i in range(np.shape(uniqueVals)[0]):
            print('Value='+str(uniqueVals[i]), ':', 'Date='+str(Start+relativedelta(days=+int(uniqueVals[i])))+'\n')
    else:
        print('PLEASE SPECIFY ONE OF THE FOLLOWING SENSORS - ALOS, ALOS2, JERS\nExample usage: python DatePredictor.py -i .../N30W092_15_date_F02DAR -s ALOS2\n')

def main():
    print('''DatePredictor.py is written by Nathan Thomas (nmthomas28@gmail.com, @Nmt28) of the NASA Jet Propulsion Laboratory. THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.\n''')

    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input", type=str, help="Specify the input ALOS-2 date raster")
    parser.add_argument("-s", "--sensor", type=str, help="Specify the input sensor (alos, alos2)")
    args = parser.parse_args()
    
    if args.input == None:
        print("SPECIFY INPUT IMAGE FILE\nExample usage: python DatePredictor.py -i .../N30W092_15_date_F02DAR -s ALOS2\n")
        os._exit(1)
    if args.sensor == None:
        print("SPECIFY INPUT SENSOR (ALOS, ALOS2, JERS)\nExample usage: python DatePredictor.py -i .../N30W092_15_date_F02DAR -s ALOS2\n")
        os._exit(1)
    
    GetDate(args)


if __name__ == "__main__":
    main()