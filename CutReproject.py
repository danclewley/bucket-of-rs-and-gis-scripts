import os.path
import sys
import rsgislib
from rsgislib import imagefilter
from rsgislib import imagecalc
from rsgislib import imageutils
from osgeo import gdal
import argparse
'''A script to clip and reproject a target image to the parameters (projection, pixel size etc) of a master file
Author: Nathan M Thomas
Email: nmt8@aber.ac.uk
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THEAUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE'''

def ClipReproject(args):
    
    # Create blank copy iamge
    mask = args.mask
    outName = args.output
    bands = args.numberofbands
    format = args.format
    print("Creating blank copy image...\n")
    
    # Check datatype of input file
    sourceDS = gdal.Open(args.input, gdal.GA_ReadOnly)
    DataType = sourceDS.GetRasterBand(1).DataType
    DTYPE = dict({1:rsgislib.TYPE_8INT,2:rsgislib.TYPE_16UINT,3:rsgislib.TYPE_16INT,4:rsgislib.TYPE_32UINT,5:rsgislib.TYPE_32INT,6:rsgislib.TYPE_32FLOAT,7:rsgislib.TYPE_64FLOAT})
    
    # Create copy image
    rsgislib.imageutils.createCopyImage(mask, outName, int(bands), 0, format, DTYPE[DataType])
    
    #Specify the file to be clipped
    inFile = args.input
    
    #Open the input and cloned dataset, populate the empty image and reproject
    inFile = gdal.Open(inFile, gdal.GA_ReadOnly)
    outFile = gdal.Open(outName, gdal.GA_Update)
    print("\nPopuating blank image and reprojecting...\n")
    
    rType = dict({'cubic':gdal.GRA_CubicSpline,'near':gdal.GRA_NearestNeighbour,'bilinear':gdal.GRA_Bilinear,'cubicspline':gdal.GRA_CubicSpline})
    
    gdal.ReprojectImage(inFile, outFile, None, None, rType[args.resampling])

    print("Thanks you for using CutProject.py")

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-m", "--mask", type=str, help="Specify the input mask")
    parser.add_argument("-i", "--input", type=str, help="Specify the dataset to clip and reproject")
    parser.add_argument("-o", "--output", type=str, help="Specify the output")
    parser.add_argument("-n", "--numberofbands", type=str, help="Specify the number of bands within the output dataset")
    parser.add_argument("-of", "--format", type=str, help="Specify the GDAL compatible output format")
    parser.add_argument("-r", "--resampling", type=str, help="Specify the GDAL resampling method (near, cubic, bilinear, cubicspline)")
    args = parser.parse_args()
    
    if args.mask == None:
        print("INPUT MASK FILE")
        os._exit(1)
    elif args.input == None:
        print("SPECIFY INPUT DATASET TO BE CLIPPED")
        os._exit(1)
    elif args.output == None:
        print("SPECIFY OUTPUT FILEPATH")
        os._exit(1)
    elif args.numberofbands == None:
        print("SPECIFY THE OUTPUT NUMBER OF BANDS")
        os._exit(1)
    elif args.format == None:
        print("SPECIFY THE OUTPUT GDAL FORMAT")
        os._exit(1)
    elif args.resampling == None:
        print("SPECIFY THE GDAL RESAMPLING FORMAT")
        os._exit(1)


    ClipReproject(args)


if __name__ == "__main__":
    main()


